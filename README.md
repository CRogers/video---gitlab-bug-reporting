A repo of assets for my Gitlab bug reporting video for the Inkscape project. 
All content holds creative commons license (cc-by) which means it can be used, 
remixed, edited, and used for commercial or non commercial purposes as long as 
the author(s) are credited.