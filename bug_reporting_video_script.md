
Inkscape is a great tool for graphics work. But like any powerful graphics software, things don't always work as expected. The good news is, you can help by reporting the issue to us!

But before reporting your issue, there are a few things you should do first.

1. Reset your User Preferences: Edit > Preferences > System > NUKE PREFS 
2. Close all Inkscape windows, and restart with a new document
3. Try and reproduce the error with as few steps as possible

4. Once you succeed, save the file - you will attach it to the 
bug report in a later step.
Note: If Inkscape crashes on the last step repeat the steps 
that cause it, but save the file before the last step.

After you have completed these tasks, simply use the following steps to report an issue you find in Inkscape:

Step 1: Log into GitLab. 

If you don't have a GitLab account, follow the steps to create one, and then log in.

Step 2: Search for your issue

Use the search bar to try and locate the problem 
you found.

If you find a bug similar to yours click on it and
give it a thumbs up. This lets developers know the
problem affects you too.

Step 3: Report a new bug
If your search did not find your problem, click the green 
"New Issue" button.

Step 4: Add a descriptive title 

Think about a simple title which includes the words you
searched for earlier. This will help others find your issue later if they are experiencing the same issue.

Step 5: Add information to the description

Below each instruction in the Description box, write in the 
requested information about your bug.

Step 6: Attach the file you saved earlier while reproducing the error 

Step 7: Re-check all information
and submit the issue

Check that all information requested is provided and
then click the Green Submit Issue button.


That's it!
Thanks for your help! Together we can make Inkscape better for everyone!


Occasionally bugs which are occurring only in a specific file (but not a new blank file) will be interesting to developers.  But if you are a beginner, please discuss the problem with more advanced users (either in a forum, IRC, or on the mailing list), before creating a  new bug report.  Beginners may wish to discuss any potential bug with other users, before reporting it.  More below.

Search the bug tracker, to make sure it has not already been reported.
If it has, you might rather post a new comment to the existing report, if you have any new or potentially helpful info to contribute.  If you find related reports during your search, which might help to clarify circumstances, you might want to take note of their numbers or links, and put them in your new report.

Even though Launchpad automatically searches, using the title you enter, it's prudent to search yourself, using other relevant search terms.  It's surprising how search terms entered as the title usually finds different results than searches from the bug index page!

Every bug report must contain the following information
A precise description of the problem (i.e. concise and accurate but include as many details as possible), in English, including:

Step-by-step instructions that dependably reproduce the issue.
Actual program behavior
Expected program behavior
If you want to learn how to write a "good" bug report that is most helpful to developers see the very insightful essay on "How to Report Bugs Effectively" by Simon Tatham (creator of PuTTY).
The Inkscape version in use (Help menu > About Inkscape, e.g. "Inkscape 0.92.3 (2405546, 2018-03-11)")
Also list all previous versions in which the problem occurred as well as any versions that did not have the problem, if you happen to know.

The operating system in use.
Bug reports should optionally include the following, if there's a chance it will help developers to understand the problem better
a test SVG file which contains everything needed to demonstrate the problem but nothing else (minimal example / testcase)
the same file in another format, in case the problem is related to opening, saving as, importing, or exporting to/from or in/as another format besides SVG

screenshots, videos, or animations showing what the problem looks like, in case a test file is either not appropriate, or not sufficient alone, to demonstrate the problem...or maybe 'before and after' screenshots, for example
the language of the Inkscape user interface and the operating system (especially if the report is related to translations, special characters in documents and filenames, etc.)

the method of installation and the precise version of the installer if the problem is related to installing, updating and/or running Inkscape
Why is all this information important?
Because developers must be able to reproduce a problem before they can begin to fix it.  In Inkscape, bugs can be specific to certain versions or certain operating systems (as well as certain combinations of those) or occasionally certain languages.  While a test file is not strictly required, or not always appropriate or needed, anything which bug reporters can do, to make it easy for developers will help.

All Inkscape developers are volunteers.  They work on Inkscape in their spare time, after their workday, playtime, family time, and personal time.  Everything bug reporters can do to make it easy and save them time, will increase the likelihood that the bug will be fixed.

Other bug reporters' responsibilities
Remain available to respond to developers requests about your report.
You'll receive email notification whenever someone posts a comment, or simply changes some category or ranking or rating about the bug report.  You may be asked to clarify something, or to provide other files, images or documentation.  Please remember that everything you can do to help developers, will increase the likelihood of a successful fix.
Be patient and remain positive.
When you report a bug, you are doing so because you want the bug fixed. We want the same! As Inkscape is developed by volunteers in their spare time it might take a while  before somebody gets to your report, though (anywhere between hours and years). In any case try to be as kind and as helpful as possible (even at times when the developers might seem a bit dull from your perspective ;-) ) as people will usually be more motivated to work on the bug and the problem will get fixed faster.

Make the bug report, or request new features
Please report any bugs you find in Inkscape in the Launchpad Bug Tracker for Inkscape.

If you're a new, or even intermediate level Inkscape user, it might be a good idea to post the problem you're having in one of the mailing lists (developer mailing list or user community mailing list), or one of the bulletin board style forums (list on this page), to discuss the problem more directly.  Or one of the two IRC channels is another option.  Often more experienced users will be able to tell you if it's a known bug, or a bug, at all. If it's a known bug, you can look it up, and learn more about it. Perhaps by discussing it, you might learn a workaround.

New features, or wish list items may also submitted via the bug tracker system. Although again, if you are a new Inkscape user, you may want to discuss your idea on the mailing list, or in other forums, to gain some understanding of how it may fit into the developers' vision for Inkscape, before you file your report/request.
